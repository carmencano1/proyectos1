using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sonidos : MonoBehaviour
{
    [SerializeField] private AudioClip[] audios;

    private AudioSource controlAudio;
    // Start is called before the first frame update
    void Start()
    {
        controlAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void SeleccionAudio(int indice, float volume){
        controlAudio.PlayOneShot(audios[indice], volume);
    }
    public void Stop(){
        controlAudio.Stop();
    }
}
