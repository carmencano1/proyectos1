
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EscribirTexto : MonoBehaviour
{
    public float tiempo;
    string frase = "Año xxx, un pueblo cuyo pasado era próspero es ahora decadente, causado por un rey tirano, ansioso de poder nubla la esperanza de un futuro. En tiempos de sombra, siempre aparece una pizca de luz que va creciendo, y TÚ, HÉROE, serás esa luz que nos ilumine el camindo.";
    public Text texto;

    // Start is called before the first frame update
    void Start()
    {
        tiempo = 0;
        StartCoroutine(Reloj());
    }

    void Update()
    {
        tiempo = tiempo + Time.deltaTime;
        if (tiempo >= 31){
            Scene();
        }    
    }

    IEnumerator Reloj()
    {
        foreach (char caracter in frase)
        {
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(0.11f);
        }
    }
    public void Scene(){
        SceneManager.LoadScene(2);
    }
}