using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Escena2 : MonoBehaviour
{
    public GameObject Escenaactual;
    public GameObject Player;
    public GameObject camara;
    public GameObject Escenasiguiente;
    private bool tocandopared2;
    public GameObject Playerhall_pos;
    public GameObject Playerhall;
    public Check_point CP;
    public Check_point CP2;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(tocandopared2 == true){
            Time.timeScale = 1f;
            Escenasiguiente.SetActive(true);
            camara.transform.position = new Vector3 (Escenasiguiente.transform.position.x, Escenasiguiente.transform.position.y, -77F);
            Player.SetActive(false);
            Playerhall.transform.position = Playerhall_pos.transform.position;
            Escenaactual.SetActive(false);
            tocandopared2 = false;
            Playerhall.SetActive(true);
        }
    }

    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.name == "Player")
        { 
        CP.Escena_actual = Escenasiguiente;
        CP2.Escena_actual = Escenasiguiente;
        tocandopared2 = true;
        }
    }
}
