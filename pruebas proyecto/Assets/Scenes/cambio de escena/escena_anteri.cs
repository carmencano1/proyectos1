using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escena_anteri : MonoBehaviour
{
    public GameObject Escenaactual;
    public GameObject Player;
    public GameObject camara;
    public GameObject Escenasiguiente;
    private bool tocandopared2;
    public GameObject Player_pos;
    public Check_point CP;
    public Check_point CP2;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if(tocandopared2 == true){
            Time.timeScale = 1f;
            Escenasiguiente.SetActive(true);
            camara.transform.position = new Vector3 (Escenasiguiente.transform.position.x, Escenasiguiente.transform.position.y, -77F);
            Player.transform.position = Player_pos.transform.position;
            Player.SetActive(true);
            Escenaactual.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.name == "Player")
        { 
            CP.Escena_actual = Escenasiguiente;
            CP2.Escena_actual = Escenasiguiente;
        tocandopared2 = true;
        }
    }
}
