using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageenemigos1 : MonoBehaviour
{
  public int espada;

  public bool Enemigomuerto;

  public GameObject spot;

  public GameObject energia;

  public GameObject player;

  private bool inpact;
  
  private Rigidbody2D rb2d;

  public GameObject m_ParentObject;

  public float vidas;

  private Animator anim;

  private float Damaged;

  private movimientoEnemigos1 me;

  private GameObject parent;

  private ActivateObject AO;

  private sonidos sonido;

  void Start(){
    me = GetComponent<movimientoEnemigos1>();
    anim = GetComponent<Animator>();
    player = GameObject.Find("Player");
    AO = player.GetComponent<ActivateObject>();
    rb2d = GetComponent<Rigidbody2D>();
    vidas = 5;
    Damaged = 0;
    parent = transform.parent.gameObject;
    espada = 1;
    sonido = GetComponent<sonidos>();
  }
  void Update(){
    espada = AO.Daño_espada;
    Damaged = Damaged + Time.deltaTime;
    StartCoroutine(Impulso()); 
    StartCoroutine(Daño());   
    if (vidas <= 0){
      GameObject newenergia = Instantiate(energia, spot.transform.position, energia.transform.rotation);
      reset();
    }
    if(Damaged >= 0.5){
      anim.SetBool("Golpeado", false);
      me.speed = 35;
    }
    
  }
    private IEnumerator Daño(){
      if (inpact == true){
        vidas = vidas-espada;
        rb2d.AddForce(-2500000f * new Vector2 (CallculateNormalizedBulledDirection().x, 0f));
        inpact = false; 
        me.speed = 0;
      }
    yield return new WaitForSeconds(1f);
  }
  void OnCollisionEnter2D(Collision2D collision){
    if (collision.gameObject.name == "Death_point"){
      vidas = 0;
    }
    if (collision.gameObject.name == "espada")
    { 
      sonido.SeleccionAudio(1, 0.5f);
      inpact = true;
      anim.SetBool("Golpeado", true);
      Damaged = 0;
      StartCoroutine(Daño());
    }
    if (collision.gameObject.name == "flecha(Clone)")
    { 
      anim.SetBool("Golpeado", true);
      sonido.SeleccionAudio(1, 0.5f);
      inpact = true;
      StartCoroutine(Daño2());
      Damaged = 0;
    }
    if (collision.gameObject.name == "temblor(Clone)")
    { 
      anim.SetBool("Golpeado", true);
      sonido.SeleccionAudio(1, 0.5f);
      inpact = true;
      StartCoroutine(Impulso());
      Damaged = 0;
    }
  }
    private IEnumerator Daño2(){
       if (inpact == true){
        vidas = vidas-1;
        rb2d.AddForce(-1250000f * new Vector2 (CallculateNormalizedBulledDirection().x, 0f));
        inpact = false;
         me.speed = 0;
       }
    yield return new WaitForSeconds(1f);
  }

  private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(player.transform.position - m_ParentObject.transform.position);
    }
  private IEnumerator Impulso(){
  if(inpact == true){
    rb2d.AddForce(-1600000f * new Vector2 (CallculateNormalizedBulledDirection().x, -0.5f));
    inpact = false;
    me.speed = 0;
  }
  yield return new WaitForSeconds(1f);
  }
  public void reset(){
    transform.position = parent.transform.position;
    vidas = 5;
    parent.SetActive(false);
  }
}
  
