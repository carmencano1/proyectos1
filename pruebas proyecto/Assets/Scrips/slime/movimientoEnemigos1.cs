using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoEnemigos1 : MonoBehaviour
{
    public Congelacion conge;
    
    public float speed;

    public Rigidbody2D rb2D;

    public bool esDerecha;

    public Vector3 velocidad = Vector3.zero;

    [SerializeField] public float suavizadoMovi = 0.2f;
    
    public bool suelo;

    [SerializeField] private LayerMask queEsSuelo;

    [SerializeField] public Transform controladorSuelo;

    [SerializeField] public Vector3 dimensionesCaja;

    private float tiempo;

    private float tiempo_sonido;

    private GameObject player;

    private sonidos sonido;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        player = GameObject.Find("Player");
        conge = player.GetComponent<Congelacion>();
        tiempo = 0;
        rb2D = GetComponent<Rigidbody2D>();
        tiempo_sonido = 0.4f;
    }
    void Update(){
        tiempo = tiempo + Time.deltaTime;
        tiempo_sonido = tiempo_sonido + Time.deltaTime;
        if(tiempo_sonido >= 0.55f){  
            sonido.SeleccionAudio(0, 0.1f);
            tiempo_sonido =0;
        }
    }
    private void FixedUpdate(){
        suelo = Physics2D.OverlapBox(controladorSuelo.position, dimensionesCaja, 0f, queEsSuelo);
        if ((tiempo >= 1f) && (suelo == false)){
            Vector3 escala = transform.localScale;
            escala.x *= -1;
            transform.localScale = escala;
            esDerecha = !esDerecha;
            tiempo = 0;
        }
        if (conge.congela == false){
        if (esDerecha){
            mover(speed);
        }
        if (!esDerecha){
            mover(-speed);
        }
        }
    }
    private void mover(float mover){
        Vector3 velocidadObjetivo = new Vector2(mover, rb2D.velocity.y);
        rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
    }
}

