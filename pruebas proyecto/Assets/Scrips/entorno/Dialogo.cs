using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogo : MonoBehaviour
{
    public Dialogos dialogos;
    public string Primera_linea;
    public string Segunda_linea;
    private bool disponible;
    public int contador;
    public GameObject Bloque_dialogo;
    void Start(){
    }
    void Update(){
        if(disponible == true){
            if(Input.GetKeyDown("f")&&(contador >= 0)){
                dialogos.dialogo(Primera_linea);
                contador = contador +1;
                if(Input.GetKeyDown("f") && ( contador>=2 )){
                    dialogos.dialogo(Segunda_linea);
                    contador = 0;
                }   
            }
            
        }   
    }
    void OnCollisionEnter2D (Collision2D collision){
        if(collision.gameObject.name == "player"){
            disponible = true;
            contador = 0;
            Bloque_dialogo.SetActive(true);
        }
    }
    void OnCollisionExit2D (Collision2D collision){
        if(collision.gameObject.name == "player"){
            disponible = false;
            Bloque_dialogo.SetActive(false);
        }
    }
}
