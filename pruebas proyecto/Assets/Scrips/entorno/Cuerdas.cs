using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuerdas : MonoBehaviour
{
    private bool roto;
    private Animator anima;
    private float tiempo;
    private GameObject grid;
    public GameObject muro;
    // Start is called before the first frame update
    void Start(){
        grid = GameObject.Find("gridtemporal");
        tiempo = 0;
        anima = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (roto == true){
            tiempo = tiempo + Time.deltaTime;
            anima.SetBool("roto", true);
        }
        if (tiempo >= 1){
            grid.SetActive(false);
            muro.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision){
        print("mi loco");
        if (collision.name == "flecha(Clone)"){
            roto = true;
        }
    }
}
