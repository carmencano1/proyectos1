using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espadas : MonoBehaviour
{
    public int espada;
    public ActivateObject AO;
    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.name == "Player"){
            AO.Daño_espada = espada;
        }
    }
}
