using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogos : MonoBehaviour
{
    public TMP_Text texto;
    public void dialogo(string txt){
        texto.text = txt;
    }
}
