using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mTopDown : MonoBehaviour
{
    public float Speed = 5f;
    Vector2 movement;
    private Rigidbody2D rb2D;
    private Animator anim;
    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void Update()
    {
        
       movement.x = Input.GetAxisRaw("Horizontal");
       movement.y = Input.GetAxisRaw("Vertical");
       if (movement.x != 0){
        movement.y = 0;
       }
       if (movement.y != 0){
        movement.x = 0;
       }
       if(movement.y > 0){
        anim.SetFloat("multiplicator", 1);
        anim.SetBool("arriba", true);
        anim.SetBool("abajo", false);
        anim.SetBool("lateral", false);
       }
       if(movement.y < 0){
        anim.SetFloat("multiplicator", 1);
        anim.SetBool("abajo", true);
        anim.SetBool("arriba", false);
        anim.SetBool("lateral", false);
       }
       if(movement.x > 0){
        anim.SetFloat("multiplicator", 1);
        this.transform.localScale = new Vector3 (61.357f,61.357f,1);
        anim.SetBool("lateral", true);
        anim.SetBool("arriba", false);
        anim.SetBool("abajo", false);
       }
       if(movement.x < 0){
        anim.SetFloat("multiplicator", 1);
        this.transform.localScale = new Vector3 (-61.357f,61.357f,-1);
        anim.SetBool("lateral", true);
        anim.SetBool("arriba", false);
        anim.SetBool("abajo", false);
       }
       if((movement.x == 0)&&(movement.y == 0)){
        anim.SetFloat("multiplicator", 0);
       }
    }
    
    private void FixedUpdate(){
        movimientopers();
    }
    void movimientopers(){
        rb2D.MovePosition(rb2D.position + movement * Speed);
    }
   
}
