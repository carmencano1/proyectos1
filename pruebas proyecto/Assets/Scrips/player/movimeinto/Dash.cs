using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{
    private sonidos sonido;
    private Animator animator;
    public GameObject player;
    public GameObject Habilidad;
    private Rigidbody2D rb2D;
    public float tiempo1;
    public GameObject enemi_coli;
    private bool endash;

    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        animator = GetComponent<Animator>();
        tiempo1 = 3;
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        tiempo1 = tiempo1 + Time.deltaTime;
        if ((Input.GetKeyDown("right shift") || (Input.GetKeyDown("left shift"))) && (tiempo1 >= 3)){
            sonido.SeleccionAudio(4, 1f);
            animator.SetBool("dash", true);
            rb2D.AddForce(-5000 * new Vector2(CallculateNormalizedBulledRotation(), 0));
            enemi_coli.SetActive(false);
            tiempo1 = 0;
            endash = true;
        }
        else if((tiempo1 >= 1)&&(endash == true)){
            animator.SetBool("dash", false);
            enemi_coli.SetActive(true);
            endash = false;
        }
    }
    private float CallculateNormalizedBulledRotation(){
        float cuenta = player.transform.position.x - Habilidad.transform.position.x;
        return cuenta; 
    }
}
