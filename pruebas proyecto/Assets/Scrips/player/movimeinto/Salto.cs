using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    private Animator animator;

    private Rigidbody2D rb2D;

    [SerializeField] private float fuerzaSalto;

    [SerializeField] private LayerMask queEsSuelo;

    [SerializeField] public Transform controladorSuelo;

    [SerializeField] public Vector3 dimensionesCaja;

    [SerializeField] private bool enSuelo;

    private bool salto = false;

    private bool tocandopared;

    [SerializeField] public Transform controladorpared;

    [SerializeField] public Vector3 dimensionesCaja2;

    [SerializeField] private LayerMask pared;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump")){
        salto = true;
        }
    }
    private void FixedUpdate(){
        enSuelo = Physics2D.OverlapBox(controladorSuelo.position, dimensionesCaja, 0f, queEsSuelo);
        tocandopared = Physics2D.OverlapBox(controladorpared.position, dimensionesCaja2, 0f, pared);
        saltar();
        salto = false;
    }
    private void saltar(){
        if (enSuelo){
            animator.SetBool("space", false);
            if(salto){
                if(tocandopared){
                    enSuelo = false;
                    rb2D.AddForce(new Vector2(0f, fuerzaSalto + 300));
                }
            enSuelo = false;
            rb2D.AddForce(new Vector2(0f, fuerzaSalto));
        }
        if(tocandopared){
            rb2D.AddForce(new Vector2(-10, -300));
        }
        }
        else if(!enSuelo){
            animator.SetBool("space", true);
        }
    }
}
