using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mLateral : MonoBehaviour
{
  private Animator animator;

  private Rigidbody2D rb2D;

  [Header ("Movimiento")]
  private float movimientoH = 0f;

  [SerializeField] private float velocidadMovi = 3500;

  [Range(0, 0.3f)][SerializeField] private float suavizadoMovi;

  private Vector3 velocidad = Vector3.zero;

  public bool mirandoDere = true;

 private void Start(){
    animator = GetComponent<Animator>();
    rb2D = GetComponent<Rigidbody2D>();
 }
 private void Update(){
    movimientoH = Input.GetAxisRaw("Horizontal") * velocidadMovi;
 }
 private void FixedUpdate(){
    Mover(movimientoH * Time.fixedDeltaTime);
 }
 private void Mover(float mover){
    if(mover != 0f){
      animator.SetBool("AD", true);
    }
    else{
      animator.SetBool("AD", false);
    }
    Vector3 velocidadObjetivo = new Vector2(mover, rb2D.velocity.y);
    rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
    if(mover > 0 && !mirandoDere){
        Girar();
    }
    else if (mover < 0 && mirandoDere){
        Girar();
    }
 }
 private void Girar(){
    mirandoDere= !mirandoDere;
    Vector3 escala = transform.localScale;
    escala.x *= -1;
    transform.localScale = escala;
 }
}
