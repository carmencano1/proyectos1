using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impulso : MonoBehaviour
{
    public bool Impacto1;
    public bool Impacto2;
    public bool Impacto3;
    void Update(){
    }
    void OnCollisionEnter2D(Collision2D collision){
    if (collision.gameObject.name == "soldier")
    { 
      StartCoroutine(Impact1());
    }
    if (collision.gameObject.name == "slime")
    { 
      StartCoroutine(Impact2());
    }
    if (collision.gameObject.name == "Mage")
    { 
      StartCoroutine(Impact3());
    }
    }
    private IEnumerator Impact1(){ 
      Impacto1 = !Impacto1;
    yield return new WaitForSeconds(1f);
        
  }
  private IEnumerator Impact2(){
      Impacto2 = !Impacto2;
    yield return new WaitForSeconds(1f);
        
  }
  private IEnumerator Impact3(){ 
      Impacto3 = !Impacto3;
    yield return new WaitForSeconds(1f);
        
  }
}
