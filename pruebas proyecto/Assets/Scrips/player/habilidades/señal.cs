using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class señal : MonoBehaviour
{
    public Transform target;

    public Transform enemigo;

    public float speed;

    private Rigidbody2D rb2D;

    public bool debeperseguir;

    public bool ignore = true;

    public float distancia;

    public Vector3 velocidad = Vector3.zero;

    public float suavizadoMovi = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate(){
        if (debeperseguir == true){
            mover(speed);
        }
        if(debeperseguir == false){
            mover(-speed);
        }

    }
    private void mover(float mover){
        Vector3 velocidadObjetivo = new Vector2 (mover, rb2D.velocity.y);
        rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
        enemigo.position = Vector3.MoveTowards(enemigo.position, target.position, speed);
    }
}
