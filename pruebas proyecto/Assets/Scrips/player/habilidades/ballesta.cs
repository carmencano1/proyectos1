using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballesta : MonoBehaviour
{
    private Animator animator;

    public Transform flecha;

    public float balaSpeed = 5f;

    public GameObject Habilidad;

    public GameObject bala;

    public Transform spot;

    public float tiempo;

    public GameObject player;

    private GameObject arco;

    private sonidos sonido;

    private AttributesController obtenido;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        arco = GameObject.Find("arco");
        obtenido = arco.GetComponent<AttributesController>();
        animator = GetComponent<Animator>();
        flecha.rotation = Quaternion.Euler(0, 0, 0);
        tiempo = 10;
    }

    // Update is called once per frame
    void Update()
    {
        tiempo = tiempo + Time.deltaTime;
        if(obtenido.cantidad >= 1){
            if ((tiempo >= 10) && Input.GetKeyDown("q")){
                animator.SetBool("b", true);
                sonido.SeleccionAudio(1, 0.5f);
                StartCoroutine(Fire());
                tiempo = 0;
            }
            else if (tiempo >= 0.5){
                animator.SetBool("b", false);
            }
        }
    }
    private IEnumerator Fire(){
        yield return new WaitForSeconds(0.5f);
        if (CallculateNormalizedBulledRotation() < 0){
            flecha.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if(CallculateNormalizedBulledRotation() > 0){
            flecha.rotation = Quaternion.Euler(0, 180, 0);
        }
        GameObject newflecha = Instantiate(bala, spot.transform.position, bala.transform.rotation);
        Rigidbody2D rb2D = newflecha.GetComponent<Rigidbody2D>();
        
        rb2D.velocity = balaSpeed * CallculateNormalizedBulledDirection();
    }
    private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(spot.transform.position - Habilidad.transform.position);
    }
    private float CallculateNormalizedBulledRotation(){
        float cuenta = player.transform.position.x - Habilidad.transform.position.x;
        return cuenta;
    }
}
