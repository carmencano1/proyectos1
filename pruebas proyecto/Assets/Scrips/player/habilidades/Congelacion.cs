using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Congelacion : MonoBehaviour
{
    private sonidos sonido;
    public float tiempo; 
    public bool congela;
    public int orbes;
    public AttributesController obtenido;
    public TMP_Text texto;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        orbes = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (obtenido.cantidad>=1){
        tiempo = tiempo + Time.deltaTime;
        if (orbes >= 10){
            if(orbes > 10){
                orbes = 10;
            }
            if(Input.GetKey("x")){
                sonido.SeleccionAudio(3, 0.5f);
                congela = true;
                orbes = 0;
                tiempo = 0;
            } 
        }
        if (tiempo >= 5){
            congela = false;
        }
        }
        texto.text = orbes + "";
    }

    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.name == "orbe(Clone)"){
            orbes = orbes + 1;
        }
    }
}
