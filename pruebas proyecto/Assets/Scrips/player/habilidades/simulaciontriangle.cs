using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simulaciontriangle : MonoBehaviour
{
    public Vector3 posicion;
    private float time;
    public Collider2D Objeto;
    public Animator animator;
    public Rigidbody2D rb2D;
    // Start is called before the first frame update
  void Start()
    {
        posicion = transform.localPosition;
        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        Objeto.enabled = false;
        rb2D.simulated = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = posicion;
        time = time + Time.deltaTime;
        if((Input.GetKey("v")) && (time >= 0.75)){
            animator.SetBool("click", true);
            Objeto.enabled = true;
            rb2D.simulated = true;
            time = 0;
        }
        else if(time >= 0.45){
            animator.SetBool("click", false);
            Objeto.enabled = false;
            rb2D.simulated = false;
        }   
        
    }
}
