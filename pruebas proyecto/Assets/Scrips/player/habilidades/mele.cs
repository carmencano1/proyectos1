using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mele : MonoBehaviour
{
    private float time; 
    private Animator animator;
    public Collider2D Objeto;
    private sonidos sonido;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        animator = GetComponent<Animator>();
        time = 0;
        Objeto.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        time= time + Time.deltaTime;
        if((Input.GetKey("v")) && (time >= 0.75)){
            sonido.SeleccionAudio(0, 0.5f);
            Objeto.enabled = true;
            animator.SetBool("click izq", true);
            time = 0;
        }
        else if(time >= 0.15){
            animator.SetBool("click izq", false);
            Objeto.enabled = false;
        }   
        
    }
}
