using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patadon : MonoBehaviour
{
    private sonidos sonido;
    private Animator animator;
    public Transform temblorpos;
    public float temblorSpeed = 5f;
    public GameObject Habilidad;
    public GameObject temblor;
    public Transform spot;
    public float tiempo;
    public GameObject player;
    public AttributesController obtenido;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        animator = GetComponent<Animator>();
        temblorpos.rotation = Quaternion.Euler(0, 0, 0);
        tiempo = 15;
    }

    // Update is called once per frame
    void Update()
    {
        tiempo = tiempo + Time.deltaTime;
        if(obtenido.cantidad >= 1){
        if ((tiempo >= 15) && Input.GetKey("e")){
            sonido.SeleccionAudio(2, 0.5f);
            animator.SetBool("Pisoton", true);
            StartCoroutine(Fire());
            tiempo = 0;
        }
        else if (tiempo >= 0.5){
            animator.SetBool("Pisoton", false);
        }
        }

    }
    private IEnumerator Fire(){
        yield return new WaitForSeconds(0.5f);
        if (CallculateNormalizedBulledRotation() < 0){
            temblorpos.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if(CallculateNormalizedBulledRotation() > 0){
            temblorpos.rotation = Quaternion.Euler(0, 180, 0);
        }
        GameObject newflecha = Instantiate(temblor, spot.transform.position, temblor.transform.rotation);
        Rigidbody2D rb2D = newflecha.GetComponent<Rigidbody2D>();
        
        rb2D.velocity = temblorSpeed * CallculateNormalizedBulledDirection();
    }
    private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(spot.transform.position - Habilidad.transform.position);
    }
    private float CallculateNormalizedBulledRotation(){
        float cuenta = player.transform.position.x - Habilidad.transform.position.x;
        return cuenta;
    }
}

