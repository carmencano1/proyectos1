using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corazones : MonoBehaviour
{
    public ActivateObject Player;

    public GameObject[] Corazon;
    
    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.vidas == 10){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(true);
            Corazon[6].SetActive(true);
            Corazon[7].SetActive(true);
            Corazon[8].SetActive(true);
            Corazon[9].SetActive(true);
        }
        if (Player.vidas == 9){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(true);
            Corazon[6].SetActive(true);
            Corazon[7].SetActive(true);
            Corazon[8].SetActive(true);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 8){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(true);
            Corazon[6].SetActive(true);
            Corazon[7].SetActive(true);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 7){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(true);
            Corazon[6].SetActive(true);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 6){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(true);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 5){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(true);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 4){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(true);
            Corazon[4].SetActive(false);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 3){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(true);
            Corazon[3].SetActive(false);
            Corazon[4].SetActive(false);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 2){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(true);
            Corazon[2].SetActive(false);
            Corazon[3].SetActive(false);
            Corazon[4].SetActive(false);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas == 1){
            Corazon[0].SetActive(true);
            Corazon[1].SetActive(false);
            Corazon[2].SetActive(false);
            Corazon[3].SetActive(false);
            Corazon[4].SetActive(false);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
        if (Player.vidas <= 0){
            Corazon[0].SetActive(false);
            Corazon[1].SetActive(false);
            Corazon[2].SetActive(false);
            Corazon[3].SetActive(false);
            Corazon[4].SetActive(false);
            Corazon[5].SetActive(false);
            Corazon[6].SetActive(false);
            Corazon[7].SetActive(false);
            Corazon[8].SetActive(false);
            Corazon[9].SetActive(false);
        }
    }
}
