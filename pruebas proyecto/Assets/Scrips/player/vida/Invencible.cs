using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invencible : MonoBehaviour
{
    public Animator animator;

    public ActivateObject AO; 

    public bool impactado;

    public float tiempo;

    public GameObject enemi_coli;

    // Start is called before the first frame update
    void Start()
    {
        tiempo = 0;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(AO.impact ==true){
            impactado = true;
            tiempo = 0;
        }
        tiempo = tiempo + Time.deltaTime;
    }
    void FixedUpdate(){
        if(impactado == true){
            animator.SetBool("Dañado", true);
            enemi_coli.SetActive(false);
            AO.impact = false;

        }
        if((tiempo >= 4)&&(impactado == true)){
            enemi_coli.SetActive(true);
            animator.SetBool("Dañado", false);
            impactado = false;
        }
    }
}
