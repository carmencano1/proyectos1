using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VibracionCamara : MonoBehaviour
{
    public float veces;

    public ActivateObject AO; 

    private bool Damaged;

    private float tiempo;

    public GameObject camara;
    void Start()
    {
        tiempo = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (AO.impact == true){
            Damaged = true;
            tiempo  = 0;
            veces = 0;
        }
        tiempo = tiempo + Time.deltaTime;
        
        if (Damaged == true){
            if((tiempo>= 0.01f)&&(veces == 0f)){
                veces = veces+1;
                Up();
            }
            if((tiempo>= 0.05f)&&(veces == 1f)){
                veces = veces+1;
                Down();
            }
            if((tiempo>= 0.10f)&&(veces == 2f)){
                veces = veces+1;
                Left();
            }
            if((tiempo>= 0.15f)&&(veces == 3f)){
                veces = veces+1;
                Right();
            }
            if((tiempo>= 0.20f)&&(veces == 4f)){
                Origin();
                Damaged = false;
            }
        }
    }
    public void Up()
    {
        camara.transform.position = new Vector3 (camara.transform.position.x, camara.transform.position.y +10, camara.transform.position.z);
    }
    public void Down(){
        camara.transform.position = new Vector3 (camara.transform.position.x, camara.transform.position.y -20, camara.transform.position.z);
    }
    public void Left(){
        camara.transform.position = new Vector3 (camara.transform.position.x -10, camara.transform.position.y +10, camara.transform.position.z);       
    }
    public void Right(){
        camara.transform.position = new Vector3 (camara.transform.position.x +20, camara.transform.position.y , camara.transform.position.z);  
    }
    public void Origin(){
        camara.transform.position = new Vector3 (camara.transform.position.x -10, camara.transform.position.y , camara.transform.position.z);  
    }
}
