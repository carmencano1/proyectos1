using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{
    public int Daño_espada;

    [SerializeField] private GameObject menuMuerte;

    public Pisoton_range PR;

    public Espadazo_range EsR;

    public Estocada_range ER;

    public Pisoton_range PR2;

    public Espadazo_range EsR2;

    public Estocada_range ER2;

    public GameObject m_ParentObject;

    public int vidas = 5;

    public bool impact;

    private Animator animator;

    public bool enemigoN;

    public Invencible dañado;

    public GameObject Check_point;

    public AttributesController AC;

    private sonidos sonido;
    
    public void Start()
    {
        sonido = GetComponent<sonidos>();
        vidas = 5;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(enemigoN == true){
            StartCoroutine(Daño());
            enemigoN = false;
        }
        if ((PR.impact ==true)||(PR2.impact ==true)){
            impact = true;
            StartCoroutine(Daño());
            PR.impact = false;
            PR2.impact =false;
        }
        if(ER.impact == true){
            impact = true;
            StartCoroutine(Daño2());
            ER.impact = false;
        }
        if((EsR.impact == true)||(EsR2.impact == true)){
            impact = true;
            StartCoroutine(Daño3());
            EsR.impact = false;
            EsR2.impact = false;
        }
        if (vidas <= 0){
            menuMuerte.SetActive(true);
            dañado.animator.SetBool("Dañado", false);
            impact = false;
            Time.timeScale = 0f;
            dañado.impactado = false;
            if(Check_point.activeSelf == true){
                m_ParentObject.SetActive(false);
            }
        }
        if(AC.sonido == true){
            sonido.SeleccionAudio(5, 0.5f);
            AC.sonido = false;
        }
  }
    private IEnumerator Daño3(){
        if (impact == true){
            sonido.SeleccionAudio(6, 0.5f);
            vidas = vidas-3;
        }
        yield return new WaitForSeconds(0.75f);
    }
    private IEnumerator Daño2(){
        if (impact == true){
            sonido.SeleccionAudio(6, 0.5f);
            vidas = vidas-2;
        }
        yield return new WaitForSeconds(0.75f);
    }
    private IEnumerator Daño(){
        if (impact == true){
            sonido.SeleccionAudio(6, 0.5f);
            vidas = vidas-1;
        }
        yield return new WaitForSeconds(0.75f);
    }
}
