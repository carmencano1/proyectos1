using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Espada_soldier : MonoBehaviour
{
    public GameObject soldado;
    private sonidos sonido;
    private Damageenemigos DE;
    // Start is called before the first frame update
    void Start()
    {
        DE = soldado.GetComponent<Damageenemigos>();
        sonido = soldado.GetComponent<sonidos>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.name == "colision enemigos"){
            DE.anim.SetBool("pegar", true);
            DE.Damaged = 0;
            sonido.SeleccionAudio(0, 0.5f);
        }
    }
}
