using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageenemigos : MonoBehaviour
{
  public int espada;
  public GameObject spot;
  public GameObject energia;
  public GameObject player;
  private bool inpact;
  private Rigidbody2D rb2d;
  public GameObject m_ParentObject;
  public float vidas;
  public impulso c;
  public Animator anim;
  private movimeintoEnemigos2 me;
  public float Damaged;
  private GameObject parent;
  private ActivateObject AO;
  private sonidos sonido;
  void Start(){
    espada = 1;
    anim = GetComponent<Animator>();
    player = GameObject.Find("Player");
    vidas = 5;
    AO = player.GetComponent<ActivateObject>();
    rb2d = GetComponent<Rigidbody2D>();
    me = GetComponent<movimeintoEnemigos2>();
    Damaged = 0;
    parent = transform.parent.gameObject;
    sonido = GetComponent<sonidos>();
  }
  void Update(){
    espada = AO.Daño_espada;
    Damaged = Damaged + Time.deltaTime;
    if (vidas <= 0){
      GameObject newenergia = Instantiate(energia, spot.transform.position, energia.transform.rotation);
      transform.position = parent.transform.position;
      vidas = 5;
      parent.SetActive(false);
    }
    if(Damaged >= 0.5){
      anim.SetBool("golpe", false);
      anim.SetBool("pegar", false);
      me.speed = 1;
    }
  }
    private IEnumerator Daño(){
      if(inpact == true){
        anim.SetBool("golpe", true);
        vidas = vidas-espada;
        rb2d.AddForce(-100000000f * new Vector2 (CallculateNormalizedBulledDirection().x, 0f));
        me.speed = 0;
        inpact = false;
        
      }
      yield return new WaitForSeconds(1f);
        
  }
  void OnCollisionEnter2D(Collision2D collision){
    if (collision.gameObject.name == "Death_point"){
      vidas = 0;
    }
    if (collision.gameObject.name == "flecha(Clone)")
    { 
      inpact = true;
      sonido.SeleccionAudio(1, 0.5f);
      anim.SetBool("golpe", true);
      Damaged = 0;
      StartCoroutine(Daño2());
    }
    if (collision.gameObject.name == "temblor(Clone)")
    { 
      inpact = true;
      anim.SetBool("golpe", true);
      sonido.SeleccionAudio(1, 0.5f);
      Damaged = 0;
      StartCoroutine(Impulso());
    }
    if (collision.gameObject.name == "espada")
    { 
      inpact = true;
      sonido.SeleccionAudio(1, 0.5f);
      anim.SetBool("golpe", true);
      Damaged = 0;
      StartCoroutine(Daño());
    }
  }
  private IEnumerator Daño2(){
      if (inpact == true){
      anim.SetBool("golpe", true);
      vidas = vidas-1;
      rb2d.AddForce(-50000000f * new Vector2 (CallculateNormalizedBulledDirection().x, 0f));
      me.speed = 0;
      inpact = false;
      
    }
    yield return new WaitForSeconds(1f);
        
  }
  private Vector3 CallculateNormalizedBulledDirection(){
    return Vector3.Normalize(player.transform.position - m_ParentObject.transform.position);
  }
  private IEnumerator Impulso(){
    if(inpact == true){
      anim.SetBool("golpe", true);
      rb2d.AddForce(-8000000f * new Vector2 (CallculateNormalizedBulledDirection().x, -0.5f));
      me.speed = 0;
      inpact = false;
      
    }
    yield return new WaitForSeconds(1f);
  }
}
  
