using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimeintoEnemigos2 : MonoBehaviour
{
    public Congelacion conge;

    public GameObject target;

    public Transform enemigo;

    public float speed;

    private Rigidbody2D rb2D;

    public Vector3 velocidad = Vector3.zero;

    public float suavizadoMovi = 0.2f;

    public bool suelo;

    [SerializeField] private LayerMask queEsSuelo;

    [SerializeField] public Transform controladorSuelo;

    [SerializeField] public Vector3 dimensionesCaja;

    private Animator animator;

    private float tiempo;

    public Vector3 misuelo;

    private bool anda;

    private sonidos sonido;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player");
        conge = target.GetComponent<Congelacion>();
        misuelo = this.transform.position;
        tiempo = 0.5f;
        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        sonido = GetComponent<sonidos>();
    }

    // Update is called once per frame
    private void FixedUpdate(){
        suelo = Physics2D.OverlapBox(controladorSuelo.position, dimensionesCaja, 0f, queEsSuelo);
        if (conge.congela == false){
            mover(speed);
            if (anda == true){
                if (CallculateNormalizedBulledDirection().x<0){
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                if(CallculateNormalizedBulledDirection().x>0){
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                }
            }else{
                if (CallculateNormalizedBulledDirection2().x<0){
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                if(CallculateNormalizedBulledDirection2().x>0){
                    transform.rotation = Quaternion.Euler(0, -180, 0);
                }
            }
        }

    }
    void Update(){
        tiempo = tiempo + Time.deltaTime;
    }
    private void mover(float mover){
        if((Mathf.Abs((enemigo.position.x - target.transform.position.x)) <= 100)&&(suelo == true) && (anda == true)){
            animator.SetBool("anda", true);
            Vector3 velocidadObjetivo = new Vector2 (mover, rb2D.velocity.y);
            rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
            enemigo.position = Vector3.MoveTowards(enemigo.position, target.transform.position, speed);
            tiempo = 0f;
        }
        else{
            if(tiempo <=0.5f){
                anda = false;
                animator.SetBool("anda", true);
                Vector3 velocidadObjetivo = new Vector2 (mover, rb2D.velocity.y);
                rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
                enemigo.position = Vector3.MoveTowards(enemigo.position, misuelo, speed);
            }else{
                animator.SetBool("anda", false);
                if (tiempo >= 1){
                anda = true;
                }
            }
        }
    }
    private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(target.transform.position - enemigo.transform.position);
    }
    private Vector3 CallculateNormalizedBulledDirection2(){
        return Vector3.Normalize(misuelo - enemigo.position);
    }
}
