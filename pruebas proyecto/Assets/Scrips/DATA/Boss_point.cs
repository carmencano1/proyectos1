using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_point : MonoBehaviour
{
    public Gestor_Habilidades GH_boss;

    public Gest_Boss2 GH_boss2;

    public Gestor GH_boss3;

    public GameObject Boss1;

    public GameObject Boss2;

    public GameObject Boss3;

    public Transform Player_pos_original;

    public ActivateObject player;

    public GameObject Player;

    public GameObject player_hub;

    public GameObject player_hub_pos;

    public GameObject Escena_actual;

    public GameObject Escena_siguiente;

    public GameObject camara;

    public GameObject Check_point;

    // Start is called before the first frame update
    void Start()
    {
        player = Player.GetComponent<ActivateObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.vidas == 0){
            Boss1.SetActive(true);
            GH_boss.vidas = 10;
            Boss2.SetActive(false);
            GH_boss2.vidas = 1;
            Boss3.SetActive(false);
            GH_boss3.Vida = 50;
            Player.transform.position = Player_pos_original.position;
            Escena_actual.SetActive(false);
            Escena_siguiente.SetActive(true);
            player_hub.SetActive(true);
            player_hub.transform.position = player_hub_pos.transform.position;
            camara.transform.position = new Vector3(Escena_siguiente.transform.position.x,Escena_siguiente.transform.position.y,-77);
            Check_point.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }

}
