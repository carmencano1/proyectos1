using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambio_checkpoint : MonoBehaviour
{
    public GameObject Check_point1;
    public GameObject Check_point2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.name == "Player"){
            Check_point1.SetActive(false);
            Check_point2.SetActive(true);
        }
    }
}
