using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Check_point : MonoBehaviour
{
    public GameObject[] enemigo = new GameObject[10];

    public GameObject[] objecto = new GameObject[10];

    public Transform Player_pos_original;

    public ActivateObject player;

    public GameObject Player;

    public GameObject player_hub;

    public GameObject player_hub_pos;

    public GameObject Escena_actual;

    public GameObject Escena_siguiente;

    public GameObject camara;

    // Start is called before the first frame update
    void Start()
    {
        player = Player.GetComponent<ActivateObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if(player.vidas == 0){
            for(int i = 0; i < enemigo.Length; i++){
                enemigo[i].SetActive(true);
                if(enemigo[i].transform.childCount > 0){
                enemigo[i].transform.GetChild(0).position = enemigo[i].transform.position;
                }
            }
            for(int i = 0; i < objecto.Length; i++){
                objecto[i].SetActive(true);
            }
            Player.transform.position = Player_pos_original.position;
            Escena_actual.SetActive(false);
            Escena_siguiente.SetActive(true);
            player_hub.SetActive(true);
            player_hub.transform.position = player_hub_pos.transform.position;
            camara.transform.position = new Vector3(Escena_siguiente.transform.position.x,Escena_siguiente.transform.position.y,-77);
        }
        
    }
}
