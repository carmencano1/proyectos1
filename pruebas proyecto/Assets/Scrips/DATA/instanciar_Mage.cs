using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instanciar_Mage : MonoBehaviour
{
    public GameObject Mage;
    public GameObject Parent;
    private Vector3 Magepos;



    void Start(){
        Parent = this.gameObject;
        Magepos = this.gameObject.transform.position;
        Instantiate(Mage, Magepos, Quaternion.identity, Parent.transform);
    }
}
