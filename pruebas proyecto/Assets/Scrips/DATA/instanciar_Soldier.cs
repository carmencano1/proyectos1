using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instanciar_Soldier : MonoBehaviour
{
    public GameObject Soldier;
    public GameObject Parent;
    private Vector3 Soldierpos;



    void Start(){
        Parent = this.gameObject;
        Soldierpos = this.gameObject.transform.position;
        Instantiate(Soldier, Soldierpos, Quaternion.identity, Parent.transform);
    }
}
