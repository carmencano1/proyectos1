using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instanciar_Slime : MonoBehaviour
{
    public GameObject Slime;
    public GameObject Parent;
    private Vector3 Slimepos;



    void Start(){
        Parent = this.gameObject;
        Slimepos = this.gameObject.transform.position;
        Instantiate(Slime, Slimepos, Quaternion.identity, Parent.transform);
    }
}
