using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Movement : MonoBehaviour
{
    public float Times;

    public Congelacion conge;

    public Transform target;

    public Transform enemigo;

    public float speed;

    private Rigidbody2D rb2D;

    private Animator animator;

    public Vector3 velocidad = Vector3.zero;

    public float suavizadoMovi = 0.2f;

    public Gest_Boss2 GEST_FASE2;

    private sonidos sonido;

    public float sonido_time;

    // Start is called before the first frame update
    void Start()
    {
        speed = 0.2f;
        Times = 0.5f;
        sonido_time = 0.5f;
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sonido = GetComponent<sonidos>();
    }

    void Update(){
        sonido_time = sonido_time + Time.deltaTime;
        Times = Times + Time.deltaTime;
        if(sonido_time >= 0.9){
            sonido.SeleccionAudio(0, 0.3f);
            sonido_time=0;
        }
    }

    private void FixedUpdate(){
        if (conge.congela == false){
        StartCoroutine(Mover(speed));
        if (CallculateNormalizedBulledDirection().x>0){
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if(CallculateNormalizedBulledDirection().x<0){
            transform.rotation = Quaternion.Euler(0, -180, 0);
        }
        }

    }
    private IEnumerator Mover(float mover){
        if (Times >= 1){
        animator.SetBool("mov", true);
        Vector3 velocidadObjetivo = new Vector2 (mover, rb2D.velocity.y);
        rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
        enemigo.position = Vector3.MoveTowards(enemigo.position, target.position, speed);
        }
        yield return new WaitForSeconds(1f);
    }

    private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(target.transform.position - enemigo.transform.position);
    }
}
