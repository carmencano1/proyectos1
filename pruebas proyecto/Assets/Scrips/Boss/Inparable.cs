using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inparable : MonoBehaviour
{
    public GameObject cambioEscena;
    public GameObject Check_point1;
    public GameObject Check_point2;
    public GameObject boss;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collision){
        if (collision.name == "Player"){
            Check_point1.SetActive(false);
            cambioEscena.SetActive(true);
            Check_point2.SetActive(true);
            boss.SetActive(false);
        }
    }
}
