using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspadazoBoss2 : MonoBehaviour
{
    private float tiempo;

    private bool prueba;

    public Boss_Movement BM;

    private Animator animator;

    public Gest_Boss2 GH;

    private sonidos sonido;

    public GameObject Espada_range;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        tiempo = 0;
        animator = GetComponent<Animator>();
        Espada_range.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(GH.Espadazo == true){
            tiempo = tiempo + Time.deltaTime;
            animator.SetBool("mov", false);
            animator.SetBool("Espadazo", true);
            sonido.SeleccionAudio(4, 0.5f);
            if (tiempo >= 0.75f){
                Espada_range.SetActive(true);
                BM.speed = 0;
                BM.Times = 0.5f;
            } 
        }
        
        
        if(GH.Espadazo == false){
            tiempo = 0;
            animator.SetBool("Espadazo", false);
            animator.SetBool("mov", true);
            Espada_range.SetActive(false);
            BM.speed = 0.2f;
            
        }
    }
}