using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gest_Boss2 : MonoBehaviour
{
    public bool muerto;
    
    public DañoPlayer a;

    private bool inpact;

    public float vidas;

    public GameObject Rango;

    public Rango ER;

    public float Habilidad;

    public bool Estocada;

    public float tiempo;

    public bool Espadazo;

    public bool Pisoton;

    public GameObject fase3;

    public float ulti;

    private Animator animator;

    public GameObject ulti_range;

    private float Fase_Change;

    private Boss_Movement BSS_M;

    public GameObject Boss3;

    public GameObject rayo;

    public GameObject deco;

    private sonidos sonido;

    public GameObject bola;
    // Start is called before the first frame update
    void Start()
    {
        Fase_Change = 0;
        BSS_M = GetComponent<Boss_Movement>();
        animator = GetComponent<Animator>();
        vidas = 1;
        tiempo = 0;
        ulti = 0;
        animator.SetBool("Fase", true);
        sonido = GetComponent<sonidos>();
        sonido.SeleccionAudio(2, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        Fase_Change = Fase_Change + Time.deltaTime;
        if(Fase_Change >= 4.5f){
        BSS_M.enabled = true;
        animator.SetBool("Fase", false);


        if (vidas <=0) {
            if(tiempo <= 0.1){
            sonido.Stop();
            sonido.SeleccionAudio(5, 0.5f);
            }
            muerto = true;
            BSS_M.enabled = false;
            ulti = 0;
            bola.SetActive(false);
            animator.SetBool("muerte", true);
            if (tiempo >= 2.3){
            deco.SetActive(true);
            Boss3.SetActive(true);
            vidas = 1;
            this.gameObject.SetActive(false);
            }
        }
        ulti = ulti + Time.deltaTime;
        tiempo = tiempo + Time.deltaTime;
        if ((tiempo >= 1) && (tiempo <=1.01f)){
        Habilidad = Randomnumber();
        }
        if ((tiempo >= 1)&&(vidas>=1)){
            if (ER.puedeAtacar == true){
                if((Habilidad <= 0.2) && (Habilidad > 0.1)){
                    Espadazo = true;
                    ER.puedeAtacar = false;
                    Rango.SetActive(false);
                }
                if((Habilidad <= 0.3) && (Habilidad > 0.2)){
                    Pisoton = true;
                    ER.puedeAtacar = false;
                    Rango.SetActive(false);
                }
            }
        }
        if ((tiempo >= 2.5f)){
            Rango.SetActive(true);
            Estocada = false;
            Espadazo = false;
            Pisoton = false;
            tiempo = 0;
        }
        if(ulti >= 60){
            animator.SetBool("ulti", true);
            bola.SetActive(true);
            sonido.SeleccionAudio(3, 0.5f);
            
            if(ulti>= 63.7f){
                rayo.SetActive(true);
            }
            if(ulti >= 64){
                ulti_range.SetActive(true);
            }
        }
    }


}
    
    float Randomnumber()
    {
        float numero = Random.value;
        return numero;
    }
}