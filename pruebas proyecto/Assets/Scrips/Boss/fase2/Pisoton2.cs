using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pisoton2 : MonoBehaviour
{
    private float tiempo;

    private bool prueba;

    public Boss_Movement BM;

    private Animator animator;

    public Gest_Boss2 GH;

    private sonidos sonido;

    public GameObject Pisoton_range;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        tiempo = 0;
        animator = GetComponent<Animator>();
        Pisoton_range.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       
        if(GH.Pisoton == true){ 
            tiempo = tiempo + Time.deltaTime;
            animator.SetBool("mov", false);
            animator.SetBool("pisoton", true);
            sonido.SeleccionAudio(1, 0.5f);
            if (tiempo >= 0.75f){
                Pisoton_range.SetActive(true);
                BM.speed = 0;
                BM.Times = 0.5f;
            } 
        }
        if((GH.Pisoton == false)&&(prueba == false)){
            tiempo = 0;
            animator.SetBool("pisoton", false);
            animator.SetBool("mov", true);
            Pisoton_range.SetActive(false);
            BM.speed = 0.2f;
            
        }
    }
}