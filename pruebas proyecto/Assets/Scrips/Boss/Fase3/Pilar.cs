using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pilar : MonoBehaviour
{
    public Transform bloqueado;
    private Animator anim;
    private Rigidbody2D rb2d;
    public Transform posicion;
    private float tiempo;
    public GameObject bloque;
    public bool parte2;
    public bool parte3;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb2d = bloque.GetComponent<Rigidbody2D>();
        anim.SetFloat("speed", 1);
        rb2d.gravityScale = 10;
    }

    // Update is called once per frame
    void Update()
    {
        
        tiempo = tiempo +Time.deltaTime;
        if(parte2 == true){
            tiempo = tiempo + Time.deltaTime;
            anim.SetFloat("speed", 2);
            rb2d.gravityScale = 20;
        }
        if(parte3 == true){
            tiempo = tiempo + Time.deltaTime;
            anim.SetFloat("speed", 4);
            rb2d.gravityScale = 40;
        }
        anim.SetBool("pilar", true);
        if (tiempo <= 1){
            this.transform.position = bloqueado.transform.position;

        }
        
        if (tiempo >= 1.05f){
            bloque.SetActive(true);

        }
        if(tiempo >= 2.5f){
            anim.SetBool("pilar", false);
            bloque.transform.position = posicion.position;
            bloque.SetActive(false);
            tiempo = 0;
            this.gameObject.SetActive(false);
        }
    }
}
