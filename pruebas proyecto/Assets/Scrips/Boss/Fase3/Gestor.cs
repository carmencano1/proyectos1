using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gestor : MonoBehaviour
{
    public GameObject colision1;
    public GameObject colision2;
    public GameObject mano1;
    public GameObject mano2;
    public GameObject pilarObj;
    public float tiempo;
    private Animator anim;
    public int Vida;
    public mano manos;
    public mano manos2;
    public Pilar pilar;
    private sonidos sonido;
    public bool muerto;
    public float tiempo_anim;
    public AudioSource sonido_ambient;
    public GameObject fin;
    public GameObject O_no;
    public GameObject player_fin;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        Vida = 50;
        anim = GetComponent<Animator>();
        tiempo =0;
        sonido.SeleccionAudio(0, 0.5f);
        tiempo_anim = 0;
    }

    // Update is called once per frame
    void Update()
    {
        tiempo = tiempo + Time.deltaTime;
        if ((Vida <= 30)&&(Vida > 0)){
            tiempo = tiempo +Time.deltaTime;
            manos2.speed = 0.4f;
            manos.speed = 0.4f;
            pilar.parte2 = true;
        }
        if ((Vida <= 10)&&(Vida > 0)){
            tiempo = tiempo +Time.deltaTime;
            manos2.speed = 0.8f;
            manos.speed = 0.8f;
            pilar.parte3 = true;
        }      
        if (Vida <= 0){
            tiempo_anim = tiempo_anim + Time.deltaTime;
            tiempo = 0;
            if(tiempo_anim <= 0.1){
                sonido.Stop();
                sonido_ambient.Stop();
                sonido.SeleccionAudio(3, 0.5f);
            }
            anim.SetBool("muerto", true);
            anim.SetBool("quieto", false);
            anim.SetBool("ataque manos", false);
            anim.SetBool("regreso manos", false);
            if((tiempo_anim >= 2.35) && (tiempo_anim < 7))
            {
                fin.SetActive(true);
                if(tiempo_anim >= 6){
                    O_no.SetActive(true);
                }
            }
            if(tiempo_anim >= 7){
                fin.SetActive(false);
                Player.SetActive(false);
                player_fin.SetActive(true);
            }
            if((tiempo_anim >= 9.5) && (tiempo_anim <= 10)){
                player_fin.transform.localScale = new Vector3(-1.3f, 1.3f, 1.3f);
            }
            if(tiempo_anim >= 10.45f){
                SceneManager.LoadScene(3);
            }
        }
        if((tiempo >= 2)&&(tiempo <= 4)){
            anim.SetBool("quieto", true);
        }
        if((tiempo >= 5)&&(tiempo <= 5.5f)){
            anim.SetBool("quieto", false);
            anim.SetBool("ataque manos", true);
            sonido.SeleccionAudio(1, 0.5f);
            colision1.SetActive(false);
            colision2.SetActive(false);
        }
        if((tiempo >= 6) && (tiempo <= 9)){
            mano1.SetActive(true);
            mano2.SetActive(true);
        }
        if(tiempo >= 11){
            anim.SetBool("ataque manos", false);
            anim.SetBool("regreso manos", true);
            colision1.SetActive(true);
            colision2.SetActive(true);
        }
        if(tiempo >= 12){
            anim.SetBool("regreso manos", false);
            anim.SetBool("quieto", true);
        }
        if((tiempo >= 14) && (tiempo <= 15)){
            anim.SetBool("quieto", false);
            pilarObj.SetActive(true);
            sonido.SeleccionAudio(2, 0.5f);
        }
        if (tiempo >= 16){
            tiempo =0;
        }
        
    }
}
