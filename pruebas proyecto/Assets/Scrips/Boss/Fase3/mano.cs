using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mano : MonoBehaviour
{
    private Rigidbody2D rb2d;

    public GameObject target;

    public Transform enemigo;

    public float speed;

    private Rigidbody2D rb2D;

    public Vector3 velocidad = Vector3.zero;

    public float suavizadoMovi = 0.2f;

    public Transform otra_pos; 

    private Vector3 guardado_target = new Vector3 (0,0,0);

    public Transform new_target;

    private Vector3 guardado_enemigo = new Vector3 (0,0,0);

    public Transform origen;

    public ActivateObject AO;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 velocidadObjetivo = new Vector2 (speed, rb2D.velocity.y);
        rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, velocidadObjetivo, ref velocidad, suavizadoMovi);
        enemigo.position = Vector3.MoveTowards(enemigo.position, target.transform.position, speed);

        if(enemigo.position == target.transform.position){
            guardado_target = target.transform.position;
            guardado_enemigo = origen.position;
            enemigo.position = otra_pos.position;
            origen.position = otra_pos.position;
            target.transform.position = new_target.position;
            new_target.position = guardado_target;
            otra_pos.position = guardado_enemigo;
            this.gameObject.SetActive(false);
        }
    }
    void OnCollisionEnter2D(Collision2D collision){
        if(collision.gameObject.name == "Player"){
            AO.vidas = AO.vidas - 1;
        }
    }
}
