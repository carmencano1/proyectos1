using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pisoton_range : MonoBehaviour
{
    public bool impact; 
    // Start is called before the first frame update
    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.name == "Player")
        { 
            impact = true;
        }
    }
}
