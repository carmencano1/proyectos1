using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estocada : MonoBehaviour
{
    public Boss_Movement BM;

    private Animator animator;

    public Gestor_Habilidades GH;

    public GameObject Estocada_range;

    private sonidos sonido;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        sonido = GetComponent<sonidos>();
        Estocada_range.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(GH.Estocada == true){
            animator.SetBool("Estocada", true);
            sonido.SeleccionAudio(2, 0.5f);
            Estocada_range.SetActive(true);
            BM.speed = 0;
            BM.Times = 0.5f;
        }
        if(GH.Estocada == false){
            animator.SetBool("Estocada", false);
            Estocada_range.SetActive(false);
            BM.speed = 0.2f;
        }
    }
}
