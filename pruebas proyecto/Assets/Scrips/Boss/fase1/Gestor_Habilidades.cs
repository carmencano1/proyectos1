using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gestor_Habilidades : MonoBehaviour
{
    public int espada;

    public bool muerto;
    
    public DañoPlayer a;

    private bool inpact;

    public float vidas;

    public GameObject Rango;

    public Rango ER;

    public float Habilidad;

    public bool Estocada;

    private float tiempo;

    public bool Espadazo;

    public bool Pisoton;

    public GameObject fase2;

    public ActivateObject AO;

    // Start is called before the first frame update
    void Start()
    {
        espada = 1;
        vidas = 10;
        tiempo = 0;
    }

    // Update is called once per frame
    void Update()
    {
        espada = AO.Daño_espada;
        StartCoroutine(Daño2());
        StartCoroutine(Daño());

        if (vidas <=0){
            muerto = true;
            fase2.SetActive(true);
            vidas = 10;
            this.gameObject.SetActive(false);
        }
        tiempo = tiempo + Time.deltaTime;
        if ((tiempo >= 1) && (tiempo <=1.01f)){
        Habilidad = Randomnumber();
        }
        if (tiempo >= 1){
            if (ER.puedeAtacar == true){
                if((Habilidad <= 0.1) && (Habilidad > 0)){
                    Estocada = true;
                    ER.puedeAtacar = false;
                    Rango.SetActive(false);
                }
                if((Habilidad <= 0.2) && (Habilidad > 0.1)){
                    Espadazo = true;
                    ER.puedeAtacar = false;
                    Rango.SetActive(false);
                }
                if((Habilidad <= 0.3) && (Habilidad > 0.2)){
                    Pisoton = true;
                    ER.puedeAtacar = false;
                    Rango.SetActive(false);
                }
            }
        }
        if ((tiempo >= 2.5f)){
            Rango.SetActive(true);
            Estocada = false;
            Espadazo = false;
            Pisoton = false;
            tiempo = 0;
        }
    }

    private IEnumerator Daño(){
       if (a.detecta3 == true){
        vidas = vidas-espada;
        a.detecta3 = !a.detecta3;
       }
    yield return new WaitForSeconds(1f);  
  }
  private IEnumerator Daño2(){
      if (inpact == true){
      vidas = vidas-1;
      inpact = false;
    }
    yield return new WaitForSeconds(1f);
  }

    float Randomnumber()
    {
        float numero = Random.value;
        return numero;
    }

    void OnCollisionEnter2D(Collision2D collision){
        if (collision.gameObject.name == "flecha(Clone)")
        { 
        inpact = true;
        StartCoroutine(Daño2());
        }
    }
}
