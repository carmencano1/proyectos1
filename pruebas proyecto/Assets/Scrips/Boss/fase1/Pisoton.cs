using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pisoton : MonoBehaviour
{
    private float tiempo;

    private bool prueba;

    public Boss_Movement BM;

    private Animator animator;

    public Gestor_Habilidades GH;

    public GameObject Pisoton_range;

    private sonidos sonido;

    
    // Start is called before the first frame update
    void Start()
    {
        sonido = GetComponent<sonidos>();
        tiempo = 0;
        animator = GetComponent<Animator>();
        Pisoton_range.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       
        if(GH.Pisoton == true){ 
            tiempo = tiempo + Time.deltaTime;
            animator.SetBool("Pisoton", true);
            if (tiempo >= 0.75f){
                Pisoton_range.SetActive(true);

                sonido.SeleccionAudio(3, 0.5f);
                BM.speed = 0;
                BM.Times = 0.5f;
            } 
        }
        if((GH.Pisoton == false)&&(prueba == false)){
            tiempo = 0;
            animator.SetBool("Pisoton", false);
            Pisoton_range.SetActive(false);
            BM.speed = 0.2f;
            
        }
    }
}