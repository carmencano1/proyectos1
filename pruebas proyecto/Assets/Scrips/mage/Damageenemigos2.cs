using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageenemigos2 : MonoBehaviour
{
  public int espada;
  public GameObject spot;
  public GameObject energia;
  public GameObject player;
  private bool inpact;
  private Rigidbody2D rb2d;
  public GameObject m_ParentObject;
  public bool Activate;
  public float vidas;
  private Animator anim;
  private float Damaged;
  private rotacion rt;
  private GameObject parent;
  private ActivateObject AO;
  private sonidos sonido;
  void Start(){
    espada = 1;
    rt = GetComponent<rotacion>();
    Damaged = 0;
    anim = GetComponent<Animator>();
    player = GameObject.Find("Player");
    AO = player.GetComponent<ActivateObject>();
    rb2d = GetComponent<Rigidbody2D>();
    vidas = 5;
    parent = transform.parent.gameObject;
    sonido = GetComponent<sonidos>();
  }
  void Update(){
    espada = AO.Daño_espada;
    Damaged = Damaged + Time.deltaTime;
    StartCoroutine(Impulso());
    StartCoroutine(Daño());   
    if (vidas <= 0){
      GameObject newenergia = Instantiate(energia, spot.transform.position, energia.transform.rotation);
      transform.position = parent.transform.position;
      vidas = 5;
      parent.SetActive(false);
    }
    if(Damaged >= 0.5){
    anim.SetBool("golpeado", false);
    }
  }
  private IEnumerator Daño(){
    if (inpact == true){
      rt.tiempo = 0;
      sonido.SeleccionAudio(1, 0.5f);
      anim.SetBool("golpeado", true);
      vidas = vidas-espada;
      rb2d.AddForce(-8000000 * new Vector2(CallculateNormalizedBulledDirection().x, 0));
      inpact = false;
    }
    yield return new WaitForSeconds(1f);
        
  }
  void OnCollisionEnter2D(Collision2D collision){
    if (collision.gameObject.name == "Death_point"){
      vidas = 0;
    }
    if (collision.gameObject.name == "espada")
    {
      anim.SetBool("golpeado", true);
      Damaged = 0;
      sonido.SeleccionAudio(1, 0.5f);
      inpact = true;
      StartCoroutine(Daño());
    }
    if (collision.gameObject.name == "flecha(Clone)")
    { 
      anim.SetBool("golpeado", true);
      Damaged = 0;
      sonido.SeleccionAudio(1, 0.5f);
      inpact = true;
      StartCoroutine(Daño2());
    }
    if (collision.gameObject.name == "temblor(Clone)")
    { 
      anim.SetBool("golpeado", true);
      Damaged = 0;
      inpact = true;
      StartCoroutine(Impulso());
    }
    }
  private IEnumerator Daño2(){
    if (inpact == true){
      rt.tiempo = 0;
      vidas = vidas-1;
      rb2d.AddForce(-5000000 * new Vector2(CallculateNormalizedBulledDirection().x, 0));
      inpact = false;
      anim.SetBool("golpeado", false);
    }
    yield return new WaitForSeconds(1f);
        
  }
  private Vector3 CallculateNormalizedBulledDirection(){
    return Vector3.Normalize(player.transform.position - m_ParentObject.transform.position);
    }
  private IEnumerator Impulso(){
  if(inpact == true){
    rt.tiempo = 0;
    anim.SetBool("golpeado", false);
    rb2d.AddForce(-8000000f * new Vector2 (CallculateNormalizedBulledDirection().x, -0.5f));
    inpact = false;
    anim.SetBool("golpeado", false);
  }
  yield return new WaitForSeconds(1f);
  }
}
  
