using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotacion : MonoBehaviour
{
    public Congelacion conge;
    public float amplitude = 30f;
    public float HabilidadSpeed = 50f;
    public float balaSpeed = 5f;
    [Header("References")][SerializeField]private GameObject Habilidad;
    public GameObject bala;
    public Transform spot;
    float currentAngles;
    float startOrientation;
    public bool clockwise = false;
    float minOrientation;
    float maxOrientation;
    public float tiempo;
    Vector3 rotation;
    public bool esDere;
    private GameObject player;
    private sonidos sonido;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        conge = player.GetComponent<Congelacion>();
        startOrientation = Habilidad.transform.eulerAngles.z;
        currentAngles = startOrientation;
        minOrientation = startOrientation - amplitude;
        maxOrientation = startOrientation + amplitude;
        rotation = new Vector3(0, 0, currentAngles);
        sonido = GetComponent<sonidos>();
    }

    // Update is called once per frame
    void Update()
    {
        if (conge.congela == false){
        tiempo += Time.deltaTime;
        if (tiempo >= 2){
            sonido.SeleccionAudio(0, 0.5f);
            Fire();
            tiempo = 0;
        }
        }
    }
    void FixedUpdate(){
        RotateHabilidad(); 
    }
    private void RotateHabilidad(){
        if(clockwise){
            currentAngles -= HabilidadSpeed * Time.fixedDeltaTime;
            if(currentAngles < minOrientation){
                clockwise = false;
            }
        }
        else{
            currentAngles += HabilidadSpeed * Time.fixedDeltaTime;
            if (currentAngles > maxOrientation){
                clockwise = true;
            }
        }
        rotation.z = currentAngles;
        Habilidad.transform.localEulerAngles = rotation;

    }
    private void Fire(){
        GameObject newbala = Instantiate(bala, spot.transform.position, bala.transform.rotation);
        Rigidbody2D rb2D = newbala.GetComponent<Rigidbody2D>();

        rb2D.velocity = balaSpeed * CallculateNormalizedBulledDirection();
    }
    private Vector3 CallculateNormalizedBulledDirection(){
        return Vector3.Normalize(spot.transform.position - Habilidad.transform.position);
    }
    private void BalanceAndDebug(){
        Debug.DrawLine(spot.transform.position, Habilidad.transform.position);
        minOrientation = startOrientation - amplitude;
        maxOrientation = startOrientation + amplitude;
    }
}