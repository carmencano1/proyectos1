using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{
    public int layer1;
    public int layer2;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Destruir_", 5);
    }
    void Update(){
         Physics2D.IgnoreLayerCollision(layer1,layer2,true);
    }

    // Update is called once per frame
    void Destruir_()
    {
        Destroy(this.gameObject);
    }
    private void OnCollisionEnter2D(Collision2D collision){
        Destruir_();
    }
}
