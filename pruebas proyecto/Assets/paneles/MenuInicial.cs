using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour

{
   
    // Start is called before the first frame update
    public void Jugar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
 
    }

    // Update is called once per frame
   public void Salir()
    {
        Debug.Log("Salir...");
        Application.Quit();
    }
}
