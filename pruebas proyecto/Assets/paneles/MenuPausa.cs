using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPausa : MonoBehaviour
{

[SerializeField] private GameObject botonPausa;

[SerializeField] private GameObject menuPausa;

public GameObject menuMuerte;

public ActivateObject Player;

private bool juegoPausado = false;

private void Update(){

if(Input.GetKeyDown(KeyCode.Escape)){

    if(juegoPausado){

        Reanudar();

    }
    else {
        Pausa();


    }
}

}

    public void Pausa()
    {
        juegoPausado = true;
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        menuPausa.SetActive(true);
        }

    // Update is called once per frame
   public void Reanudar()
    {
        juegoPausado = false;
        Time.timeScale = 1f;
          botonPausa.SetActive(true);
        menuPausa.SetActive(false);

    }
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        Player.vidas = 5;
        menuPausa.SetActive(false);
        menuMuerte.SetActive(false);

    }
       public void Menu()
    {
        Debug.Log("Menu...");
        SceneManager.LoadScene(0);
 
    }
}
