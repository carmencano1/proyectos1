
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EscribirTexto : MonoBehaviour
{
    string frase = "Año xxx, un pueblo cuyo pasado era próspero es ahora decadente, causado por un rey tirano, ansioso de poder nubla la esperanza de un futuro. En tiempos de sombra, siempre aparece una pizca de luz que va creciendo, y tú, héroe, serás esa luz que nos ilumine el camindo.";
    public Text texto;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Reloj());
    }

    IEnumerator Reloj()
    {
        foreach (char caracter in frase)
        {
            texto.text = texto.text + caracter;
            yield return new WaitForSeconds(0.08f);
        }
    }
}